<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Mike's Homepage</title>
        <link rel="stylesheet" type="text/css" href="css/base.css" />
    </head>
    <body>
        <header>
            <h1>Mike's Homepage</h1>
        </header>

        <nav>
            <ul>
                <li><a href="/">Homepage</a></li>
                <li><a href="#">Loop Demo</a></li>
                <li><a href="#">Countdown</a></li>
            </ul>
        </nav>

        <main>
            <img src="img/mike.jpg" alt="Mike Image"/>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla nec est tortor. Fusce ut turpis orci. Mauris rhoncus metus ac sollicitudin gravida. Etiam feugiat lorem quis arcu efficitur, at bibendum velit aliquet. Nulla sollicitudin venenatis nunc, non ultricies ipsum interdum in. Nulla lacinia, quam nec commodo convallis, sem nisl scelerisque neque, ultrices mollis felis lectus eget ipsum. Etiam sit amet ullamcorper leo. Phasellus luctus elit sem, ac scelerisque elit scelerisque eu. Nullam a ornare dui. Sed et metus at lectus commodo auctor. Sed ornare ex consequat, accumsan libero et, congue ligula. Donec vel aliquet ligula, non sodales dolor.</p>

        </main>

        <footer>
            &copy; <?= date("Y") ?> Mike Gumtow
        </footer>
    </body>
</html>